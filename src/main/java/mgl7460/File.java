package mgl7460;

public class File {
	int type;
	String abspath;
	String relpath;
	String basename;
	
	public File(String path) {
		this.relpath = path;
		this.abspath = getAbsPath();
		this.basename = getBasename();
		setType();
	}
	
	public String getAbsPath() {
		return "string";
	}
	
	public void setType() {
		//Get type here
	}
	
	public int Size() {
		return 0;
	}
	
	public String LastModification() {
		return "";
	}
	
	public String getBasename() {
		return "base";
	}
	
	public String toString() {
		return this.Size() + " " + this.LastModification() + " " + this.relpath;
	}
}
